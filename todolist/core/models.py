# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)

class Item(models.Model):
    link_id = models.ForeignKey(User, on_delete=models.CASCADE)
    STATUS = (
        ('N', 'New'),
        ('D', 'Done'),
    )
    PRIORITIES = (
        ('L', 'Low'),
        ('M', 'Medium'),
        ('H', 'High'),
    )
    statut = models.CharField(max_length=1, choices=STATUS, blank="N")
    priority = models.CharField(max_length=1, choices=PRIORITIES, blank ="L")
    date = models.DateTimeField(auto_now_add=True, auto_now=False,
                                verbose_name="Date de parution")
    title = models.CharField(max_length=100, blank=True)

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
