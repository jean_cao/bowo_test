# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.views.generic.edit import UpdateView

from todolist.core.forms import SignUpForm
from todolist.core.forms import AddItem
from .models import Item


@login_required
def home(request):
    high_p = Item.objects.filter(priority='H', link_id=request.user).exclude(statut='D').values()
    medium_p = Item.objects.filter(priority='M', link_id=request.user).exclude(statut='D').values()
    low_p = Item.objects.filter(priority='L', link_id=request.user).exclude(statut='D').values()
    done_p = Item.objects.filter(statut='D', link_id=request.user).exclude(statut='N').values()
    if request.method == 'POST':
        form = AddItem(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.link_id = request.user
            item.save()
    else:
        form = AddItem()
    return render(request, 'home.html', {'high' : high_p, 'medium' : medium_p, 'low' : low_p, 'form': form})

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)

            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})
