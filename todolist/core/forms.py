from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Item
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions, StrictButton
from crispy_forms.layout import Layout, Submit


class SignUpForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout = Layout(
            'username',
            'password1',
            'password2',
            Submit('Sign in', 'Submit', css_class='btn-primary center-block'),
        )
    class Meta:
        model = User
        fields = ('username', 'password1', 'password2')

class AddItem(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout = Layout(
            'priority',
            'title',
            Submit('Sign in', 'Add a Todo', css_class='btn-primary center-block'),
        )

    class Meta:
        model = Item
        widgets = {
          'title': forms.Textarea(attrs={'rows':4, 'cols':15}),
        }
        fields = ('priority', 'title')
        exclude = ["link_id"]
